FROM canaantt/r-shiny-base:latest

# RUN apt-get update
# RUN apt-get -y install curl libxml2 libxml2-dev libcurl4-openssl-dev libssl-dev r-cran-openssl 
# ADD installRpackages.R /tmp

COPY ./ tmp
WORKDIR tmp

EXPOSE 7775
# R -e "shiny::runApp('~/shinyapp')"
CMD ["Rscript", "start.R"]




